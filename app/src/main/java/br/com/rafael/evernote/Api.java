package br.com.rafael.evernote;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Api {

    @GET("/")
    Call<List<Note>> listNotes();

    @POST("/create")
    Call<Note> createNote(@Body Note note);

    @GET("/{id}")
    Call<Note> getNote(@Path("id") int id);
}
